package net.nubsis.jadeaux;

import jade.wrapper.ControllerException;
import jade.wrapper.StaleProxyException;
import net.nubsis.jadeaux.platform.PlatformListener;

public interface JadeAuxInterface {
	void addPlatformListener(PlatformListener listener) throws ControllerException;
	
	Agent createAgent(String agentName, Class<? extends jade.core.Agent> agentClass,  Container c, Object... args) throws StaleProxyException;
	Agent createAgent(Class<? extends jade.core.Agent> agent, Container c, Object... args) throws StaleProxyException;
	Container createContainer();
	Container createContainer(String name);
	void init(String ip, int port) throws ControllerException;
	void setup() throws StaleProxyException;
}
