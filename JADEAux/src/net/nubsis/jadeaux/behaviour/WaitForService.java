package net.nubsis.jadeaux.behaviour;

import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.TickerBehaviour;

import java.util.Collection;

import net.nubsis.jadeaux.services.Service;
import net.nubsis.jadeaux.services.ServiceSubscriber;
import net.nubsis.jadeaux.services.SubscribingAgent;
import net.nubsis.jadeaux.util.Services;

public class WaitForService extends TickerBehaviour {

    private static final long serialVersionUID = -7298724269997267231L;
    private final long period;
    Service service;
    ServiceSubscriber subscriber;

    public WaitForService(SubscribingAgent agent, Service service)
    {
        this(agent, service, agent);
    }

    public WaitForService(Agent agent, Service service, ServiceSubscriber subscriber)
    {
        this(agent, service, subscriber, 100L);
    }

    public WaitForService(SubscribingAgent agent, Service service, long period)
    {
        this(agent, service, agent, period);
    }

    public WaitForService(Agent agent, Service service, ServiceSubscriber subscriber, long period)
    {
        super(agent, period);
        this.service = service;
        this.subscriber = subscriber;
        this.period = period;
        System.out.println("Waiting for service: " + Services.getUniqueNameFor(service));
    }

    @Override
    protected void onTick() {
        Collection<AID> providers = Services.getProviders(myAgent, service);
        System.out.print(".");
        if (!providers.isEmpty()) {
            subscriber.serviceFound(service, providers.toArray(new AID[providers.size()]));
            this.stop();
        }
    }

}
