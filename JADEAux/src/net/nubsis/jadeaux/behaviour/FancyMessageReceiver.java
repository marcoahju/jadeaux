package net.nubsis.jadeaux.behaviour;

import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.DataStore;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import jade.proto.states.MsgReceiver;
import net.nubsis.jadeaux.util.Templates;

public abstract class FancyMessageReceiver extends MsgReceiver {

    private static final long serialVersionUID = 4587682068161105971L;

    public FancyMessageReceiver(Agent agent)
    {
        this(agent, agent.getAID(), agent.getAID());
    }

    /**
     * Behaviour that accepts all messages sent to a specific agent
     * 
     * @param agent
     *            The agent whose messages to catch
     * @param msgKey
     *            The key to match against those messages
     */
    public FancyMessageReceiver(Agent agent, Object msgKey)
    {
        this(agent, msgKey, agent.getAID());
    }

    public FancyMessageReceiver(Agent agent, MessageTemplate template)
    {
        this(agent, template, 500L, new DataStore(), agent.getAID());
    }

    public FancyMessageReceiver(Agent agent, Object msgKey, AID... aids)
    {
        this(agent, Templates.to(agent.getAID()), 500L, new DataStore(), msgKey);
    }

    public FancyMessageReceiver(Agent agent, MessageTemplate mt, long deadline, DataStore s,
                    Object msgKey)
    {
        super(agent, mt, deadline, s, msgKey);
    }

    @Override
    protected abstract void handleMessage(ACLMessage msg);

}
