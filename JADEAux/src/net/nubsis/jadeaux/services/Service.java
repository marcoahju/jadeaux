package net.nubsis.jadeaux.services;

import java.io.Serializable;
import java.util.Collection;

/**
 * Interface for classes that represent an inter-agent Service.
 * Typically, this interface is implemented by enums that enumerate
 * the services of an application.
 * 
 * @author Nubsis
 *
 */
public interface Service extends Serializable {
	
	public static String MESSAGE_TYPE = "message-type";
	
	/**
	 * Get the name of this service. It doesn't need to be unique.
	 * @return The name of this service
	 */
	public String getName ();
	public Collection<String> getParameters();
}
