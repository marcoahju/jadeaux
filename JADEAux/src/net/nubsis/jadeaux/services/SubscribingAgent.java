package net.nubsis.jadeaux.services;

import jade.core.Agent;

public abstract class SubscribingAgent extends Agent implements
		ServiceSubscriber {

	private static final long serialVersionUID = 6823706311875960804L;

}
