package net.nubsis.jadeaux.services;

import jade.core.AID;

public interface ServiceSubscriber {
	public void serviceFound(Service service, AID... providers);
}
