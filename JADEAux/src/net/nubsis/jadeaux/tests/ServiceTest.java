package net.nubsis.jadeaux.tests;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Collection;

import net.nubsis.jadeaux.services.Service;
import net.nubsis.jadeaux.util.Services;

import org.junit.Test;

public class ServiceTest {

	private static enum First implements Service {
		ONE, TWO;


		@Override
		public Collection<String> getParameters() {
			return null;
		}

		@Override
		public String getName() {
			// TODO Auto-generated method stub
			return this.name();
		}
	}

	private enum Second implements Service {
		ONE, TWO;

		@Override
		public Collection<String> getParameters() {
			return null;
		}

		@Override
		public String getName() {
			// TODO Auto-generated method stub
			return this.name();
		}
	}

	@Test
	public void UniqueServices() {
		assertThat(First.ONE.getName(), is(not(Second.ONE.getName())));
	}

	@Test
	public void Serializable() throws IOException, ClassNotFoundException
	{
		// Write the profile object to a byte array
		Service p = First.ONE;
		ByteArrayOutputStream out = new ByteArrayOutputStream(128);
		ObjectOutputStream oos = new ObjectOutputStream(out);
		
		oos.writeObject(p);
		byte[] pArr = out.toByteArray();
		
		out = new ByteArrayOutputStream(128);
		oos = new ObjectOutputStream(out);
		
		oos.writeObject(Second.ONE);
		byte[] tArr = out.toByteArray();
				
		// Read a profile from a byte array
		ByteArrayInputStream in = new ByteArrayInputStream(pArr);
		ObjectInputStream ois = new ObjectInputStream(in);
		Service q = (First) ois.readObject();
		
		in = new ByteArrayInputStream(tArr);
		ois = new ObjectInputStream(in);
		Service t = (Second) ois.readObject();
				
		// Assert that they are the same
		assertEquals(p, q);
		assertThat(q, is(not(t)));
		assertThat(p, is(not(t)));
		
	}
}
