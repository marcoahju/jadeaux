package net.nubsis.jadeaux.tests;

import jade.wrapper.ControllerException;
import net.nubsis.jadeaux.JadeAux;

import org.junit.Test;

public class JadeAuxTest {
	@Test
	public void startupAndShutdown() throws ControllerException
	{
		JadeAux.init();
		JadeAux.shutdown();
	}
}
