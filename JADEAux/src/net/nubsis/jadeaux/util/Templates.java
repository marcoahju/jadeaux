package net.nubsis.jadeaux.util;

import jade.core.AID;
import jade.core.Agent;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import jade.lang.acl.MessageTemplate.MatchExpression;
import net.nubsis.jadeaux.messaging.MessageType;
import net.nubsis.jadeaux.messaging.Performative;
import net.nubsis.jadeaux.messaging.Protocol;
import net.nubsis.jadeaux.services.Service;

public class Templates {
	public static MessageTemplate to(AID... ids) {
		return MessageTemplate.MatchReceiver(ids);
	}

	public static MessageTemplate to(Agent... agents) {
		AID[] aids = new AID[agents.length];
		for (int i = 0; i < agents.length; i++) {
			aids[i] = agents[i].getAID();
		}

		return to(aids);
	}

	public static MessageTemplate from(Agent a) {
		return from(a.getAID());
	}

	public static MessageTemplate from(AID id) {
		return MessageTemplate.MatchSender(id);
	}

	public static MessageTemplate performative(Performative performative) {
		return MessageTemplate.MatchPerformative(performative.code());
	}

	public static MessageTemplate protocol(Protocol protocol)
	{
		return MessageTemplate.MatchProtocol(protocol.getString());
	}

	/** Creates a Template for a user defined parameter */
	public static MessageTemplate userDefined(final String key, final String value)
	{
		MatchExpression me = new MatchExpression() {
			private static final long serialVersionUID = 1L;

			@Override
			public boolean match(ACLMessage msg) {
				String t = (String) msg.getAllUserDefinedParameters().get(
								key);
				return t.equals(value);
			}
		};

		return new MessageTemplate(me);

	}

	public static MessageTemplate type(final MessageType<? extends Service> type) {
		assert type!=null;
		return userDefined(MessageType.MESSAGE_TYPE_KEY, type.getName());
	}

	/**
	 * Combine a series of message templates with conjunctions (and)
	 * 
	 * @param t
	 *            The first message template
	 * @param s
	 *            The second message templates
	 * @param templates
	 *            Moar templates!
	 * @return
	 */
	public static MessageTemplate allOf(MessageTemplate t,
					MessageTemplate... templates) {
		MessageTemplate template = t;
		if (templates.length > 0) {
			for (int i = 0; i < templates.length; i++) {
				MessageTemplate.and(template, templates[i]);
			}
		}
		return template;
	}

	/**
	 * Combine a series of message templates with disjuctions (or)
	 * 
	 * @param t
	 *            The first message template
	 * @param s
	 *            The second message templates
	 * @param templates
	 *            Moar templates!
	 * @return
	 */
	public static MessageTemplate anyOf(MessageTemplate t,
					MessageTemplate... templates) {

		MessageTemplate template = t;
		if (templates.length > 0) {
			for (int i = 0; i < templates.length; i++) {
				MessageTemplate.or(template, templates[i]);
			}
		}
		return template;
	}
}
