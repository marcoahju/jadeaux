package net.nubsis.jadeaux.util;

import jade.core.AID;
import jade.core.Agent;
import jade.domain.DFService;
import jade.domain.FIPAException;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import net.nubsis.jadeaux.messaging.MessageType;
import net.nubsis.jadeaux.services.Service;

/**
 * This class provides various {@link MuseumService} functionality for
 * {@link Agent}s.
 * 
 * @author Lukas Mattsson, Marco Ahumada Juntunen
 */
public class Services {

	private static final Map<Service, DFAgentDescription> services = new HashMap<>();
	private static final Map<AID, DFAgentDescription> agents = new HashMap<>();
	private static final Map<String, Service> serviceNames = new HashMap<>();

	private static final Set<Agent> registeredProviders = new HashSet<>();

	/**
	 * List all providers of a certain service
	 * 
	 * @param whosAsking
	 *            the agent who is asking for a list of providers.
	 * @param service
	 *            the service to ask for.
	 * @return a list of agents that can provide the wanted service.
	 */
	public static List<AID> getProviders(Agent whosAsking, Service service) {
		List<AID> providers = new ArrayList<>();

		try {
			DFAgentDescription[] candidates = DFService.search(whosAsking,
							getDescriptionFor(service));
			for (DFAgentDescription agent : candidates) {
				providers.add(agent.getName());
			}
		} catch (FIPAException e) {
			e.printStackTrace();
		}
		return providers;
	}

	/**
	 * @param whosAsking
	 *            the agent who is asking for a list of services.
	 * @return a complete list of the services available at this DF.
	 */
	public static List<Service> getAllServices(Agent whosAsking) {
		return getServices(whosAsking, null);
	}

	/**
	 * @param whosAsking
	 *            the agent who is asking for a list of services.
	 * @param agent
	 *            the agent to get a list of services from.
	 * @return a list of services provided by the given agent.
	 */
	public static List<Service> getServices(Agent whosAsking, AID agent) {
		List<Service> services = new ArrayList<>();

		try {
			DFAgentDescription[] descriptions = DFService.search(whosAsking,
							getDescriptionFor(agent));
			for (DFAgentDescription d : descriptions) {
				List<Service> available = getServicesFrom(d);
				services.addAll(available);
			}
		} catch (FIPAException e) {
			// e.printStackTrace();
		}
		return services;
	}

	/**
	 * @param agentDescription
	 *            the agent description to extract a service list from.
	 * @return a list of all the services provided by the agent denoted by the
	 *         give description.
	 */
	public static List<Service> getServicesFrom(DFAgentDescription agentDescription) {
		List<Service> services = new ArrayList<>();

		Iterator<?> retardedLowLevelIterator = agentDescription.getAllServices();
		while (retardedLowLevelIterator.hasNext()) {
			Object something = retardedLowLevelIterator.next();
			if (something instanceof ServiceDescription) {
				ServiceDescription description = (ServiceDescription) something;
				Service service = serviceNames.get(description.getType());
				if (service != null) {
					services.add(service);
				}
			}
		}
		return services;
	}

	/**
	 * Register the given {@link Agent} as a provider of the given
	 * {@link MuseumService}.
	 * 
	 * @param provider
	 *            the {@link Agent} which provides the service.
	 * @param service
	 *            the {@link MuseumService} that the agent provides.
	 */
	public static void registerService(Agent provider, Service service) {
		List<Service> providedServices = getServices(provider, provider.getAID());
		providedServices.add(service);
		boolean okSoFar = deregister(provider);
		if (!okSoFar) {
			throw new RuntimeException("Something went horribly wrong while registering "
							+ provider.getLocalName() + " for service " + service);
		}
		registerInternal(provider, providedServices);
		serviceNames.put(getUniqueNameFor(service), service);
	}

	public static void registerServices(Agent provider, Service... services) {
		for (Service s : services) {
			registerService(provider, s);
		}
	}

	/**
	 * Deregisters the given {@link Agent} from all the previously provided
	 * services.
	 * 
	 * @param provider
	 *            the {@link Agent} which no longer provides services.
	 * @return <code>true</code> if successful, <code>false</code> if the
	 *         operation failed.
	 */
	public static boolean deregister(Agent provider) {
		if (registeredProviders.contains(provider)) {
			try {
				DFService.deregister(provider);
			} catch (FIPAException e) { // serious error at this stage.
				e.printStackTrace();
				return false;
			} finally {
				registeredProviders.remove(provider);
			}
		}
		return true;
	}

	/**
	 * @param provider
	 *            the provider of services.
	 * @param services
	 *            the services the agent provides.
	 */
	private static void registerInternal(Agent provider, List<Service> services) {
		DFAgentDescription agentServices = packServiceList(provider, services);
		try {
			DFService.register(provider, agentServices);
		} catch (FIPAException e) { // serious error at this stage.
			throw new RuntimeException(e);
		}
		registeredProviders.add(provider);
	}

	/**
	 * @param provider
	 *            the {@link Agent} which provides some services.
	 * @param services
	 *            the services it provides.
	 * @return a {@link DFAgentDescription} stating that the given {@link Agent}
	 *         indeed provides the given {@link MuseumService}s.
	 */
	private static DFAgentDescription packServiceList(Agent provider, List<Service> services) {
		DFAgentDescription description = new DFAgentDescription();
		description.setName(provider.getAID());

		for (Service s : services) {
			ServiceDescription d = new ServiceDescription();
			d.setName(provider.getLocalName());
			d.setType(getUniqueNameFor(s));
			description.addServices(d);
		}
		return description;
	}

	/**
	 * @param agent
	 *            the agent we need to describe.
	 * @return a description of the agent.
	 */
	private static DFAgentDescription getDescriptionFor(AID agent) {
		if (agent == null) {
			return new DFAgentDescription();
		}

		DFAgentDescription cached = agents.get(agent);
		if (cached != null) {
			return cached;
		}

		ServiceDescription desc = new ServiceDescription();
		desc.setName(agent.getLocalName());

		DFAgentDescription agentDesc = new DFAgentDescription();
		agentDesc.setName(agent);

		agents.put(agent, agentDesc);
		return agentDesc;
	}

	/**
	 * @param service
	 *            the service we need to describe.
	 * @return a description of the service.
	 */
	private static DFAgentDescription getDescriptionFor(Service service) {
		DFAgentDescription cached = services.get(service);
		if (cached != null) {
			return cached;
		}

		ServiceDescription desc = new ServiceDescription();
		desc.setType(getUniqueNameFor(service));

		DFAgentDescription agentDesc = new DFAgentDescription();
		agentDesc.addServices(desc);

		services.put(service, agentDesc);
		return agentDesc;
	}

	public static String getUniqueNameFor(Service service) {
		return service.getClass().getName() + ":" + service.getName();
	}

	public static String getUniqueNameFor(MessageType<?> type) {
		return type.getClass().getName() + ":" + type.getName();
	}
}
