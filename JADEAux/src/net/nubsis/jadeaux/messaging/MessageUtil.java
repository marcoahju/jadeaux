package net.nubsis.jadeaux.messaging;

import jade.core.AID;
import jade.core.Agent;
import jade.lang.acl.ACLMessage;

import java.util.Iterator;

public class MessageUtil {

	public static boolean isMessageAdressedTo(ACLMessage msg, Agent a) {
		Iterator<?> receivers = msg.getAllReceiver();
		while(receivers.hasNext())
		{
			AID r = (AID) receivers.next();
			if(r.equals(a.getAID()))
				return true;
		}
		return false;
	}
}
