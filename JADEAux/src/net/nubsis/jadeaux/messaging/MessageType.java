package net.nubsis.jadeaux.messaging;

import java.util.Collection;

import net.nubsis.jadeaux.services.Service;

public interface MessageType<T extends Service> {
	public static final String MESSAGE_TYPE_KEY = "MESSAGE-TYPE";
	public String getName();
	public Collection<String> getParameters();
	public T getService();
}
