package net.nubsis.jadeaux.messaging;

import jade.domain.FIPANames.InteractionProtocol;

public enum FipaProtocol implements Protocol{
	BROKERING(InteractionProtocol.FIPA_BROKERING),
	CONTRACT_NET(InteractionProtocol.FIPA_CONTRACT_NET),
	DUTCH_AUCTION(InteractionProtocol.FIPA_DUTCH_AUCTION),
	ENGLISH_AUCTION(InteractionProtocol.FIPA_ENGLISH_AUCTION),
	ITERATED_CONTRACT_NET(InteractionProtocol.FIPA_ITERATED_CONTRACT_NET),
	PROPOSE(InteractionProtocol.FIPA_PROPOSE),
	QUERY(InteractionProtocol.FIPA_QUERY),
	RECRUTING(InteractionProtocol.FIPA_RECRUITING),
	REQUEST(InteractionProtocol.FIPA_REQUEST),
	REQUEST_WHEN(InteractionProtocol.FIPA_REQUEST_WHEN),
	SUBSCRIBE(InteractionProtocol.FIPA_SUBSCRIBE),
	ITERATED_REQUEST(InteractionProtocol.ITERATED_FIPA_REQUEST);

	private final String string;

	FipaProtocol(String s)
	{
		string = s;
	}

	@Override
	public String getString() {
		// TODO Auto-generated method stub
		return null;
	}

}
