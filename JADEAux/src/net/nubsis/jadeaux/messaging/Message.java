package net.nubsis.jadeaux.messaging;

import jade.core.AID;
import jade.core.Agent;
import jade.lang.acl.ACLMessage;

import java.util.Map;
import java.util.TreeMap;

import net.nubsis.jadeaux.services.Service;

public class Message<S extends Service> {
	
	private S service;
	private Map<String, String> parameters;
	private MessageType<S> type;
	
	public Message(MessageType<S> type)
	{
		this.service = type.getService();
		this.type = type;
		parameters = new TreeMap<>();
	}
	
	public void setParameter(String parameter, Object value)
	{
		assert(type.getParameters().contains(parameter));
		this.parameters.put(parameter, value.toString());
	}
	
	public String getParameter(String parameter)
	{
		return this.parameters.get(parameter);
	}

	public MessageType<S> getType() {
		return this.type;
	}
	
	public void send(Agent from, Performative performative, AID... recipient)
	{	
		ACLMessage msg = new ACLMessage(performative.code());
		for(AID r : recipient)
			msg.addReceiver(r);
		msg.addReplyTo(from.getAID());
		msg.addUserDefinedParameter(MessageType.MESSAGE_TYPE_KEY, type.getName());		
		for(Map.Entry<String, String> param : parameters.entrySet())
		{
			msg.addUserDefinedParameter(param.getKey(), param.getValue());
		}
		from.send(msg);
	}
	
	public void sendWithReturnAdress(Agent from, AID returnTo, Performative performative, AID... recipient)
	{
		ACLMessage msg = new ACLMessage(performative.code());
		for(AID r : recipient)
			msg.addReceiver(r);
		msg.addReplyTo(returnTo);
		msg.addUserDefinedParameter(MessageType.MESSAGE_TYPE_KEY, type.getName());		
		for(Map.Entry<String, String> param : parameters.entrySet())
		{
			msg.addUserDefinedParameter(param.getKey(), param.getValue());
		}
		from.send(msg);
		System.out.println("Send message to " + recipient[0]);
	}
}
