package net.nubsis.jadeaux.messaging;

import jade.lang.acl.ACLMessage;

/**
 * Enumerates the FIPA performatives as well as DOCUMENTING THEM! Wraps the
 * available int options of {@link ACLMessage}.
 * 
 * @author Lukas Mattsson, Marco Ahumada Juntunen
 */
public enum Performative {

    /**
     * The action of accepting a previously submitted proposal to perform an
     * action.
     */
    ACCEPT_PROPOSAL(ACLMessage.ACCEPT_PROPOSAL, "ACCEPT-PROPOSAL"),

    /**
     * The action of agreeing to perform a requested action made by another
     * agent. The Agent will carry it out.
     */
    AGREE(ACLMessage.AGREE, "AGREE"),

    /**
     * Agent wants to cancel a previous request.
     */
    CANCEL(ACLMessage.CANCEL, "CANCEL"),

    /**
     * The sender wants the receiver to perform some action as soon as some
     * proposition becomes true and thereafter each time the proposition becomes
     * true again.
     */
    CFP(ACLMessage.CFP, "CFP"),

    /**
     * The sender confirms to the receiver the truth of the content. The sender
     * initially believed that the receiver was unsure about it.
     */
    CONFIRM(ACLMessage.CONFIRM, "CONFIRM"),

    /**
     * The sender confirms to the receiver the falsity of the content.
     */
    DISCONFIRM(ACLMessage.DISCONFIRM, "DISCONFIRM"),

    /**
     * Tells the other agent that a previously requested action failed.
     */
    FAILURE(ACLMessage.FAILURE, "FAILURE"),

    /**
     * Tell another agent something. The sender must believe in the truth of the
     * statement. Most used performative.
     */
    INFORM(ACLMessage.INFORM, "INFORM"),

    /**
     * Used as content of request to ask another agent to tell us is a statement
     * is true or false.
     */
    INFORM_IF(ACLMessage.INFORM_IF, "INFORM-IF"),

    /**
     * Like {@link #INFORM_IF} but asks for the value of the expression.
     */
    INFORM_REF(ACLMessage.INFORM_REF, "INFORM-REF"),

    /**
     * Sent when the agent did not understand the message.
     */
    NOT_UNDERSTOOD(ACLMessage.NOT_UNDERSTOOD, "NOT-UNDERSTOOD"),

    /**
     * Asks another agent to forward this same propagate message to others.
     */
    PROPAGATE(ACLMessage.PROPAGATE, "PROPAGATE"),

    /**
     * Used as a response to a {@link #CFP}. Agent proposes a deal.
     */
    PROPOSE(ACLMessage.PROPOSE, "PROPOSE"),

    /**
     * The sender wants the receiver to select target agents denoted by a given
     * description and to send an embedded message to them.
     */
    PROXY(ACLMessage.PROXY, "PROXY"),

    /**
     * The action of asking another agent whether or not a given proposition is
     * true.
     */
    QUERY_IF(ACLMessage.QUERY_IF, "QUERY-IF"),

    /**
     * The action of asking another agent for the object referred to by an
     * referential expression.
     */
    QUERY_REF(ACLMessage.QUERY_REF, "QUERY-REF"),

    /**
     * The action of refusing to perform a given action, and explaining the
     * reason for the refusal.
     */
    REFUSE(ACLMessage.REFUSE, "REFUSE"),

    /**
     * The action of rejecting a proposal to perform some action during a
     * negotiation.
     */
    REJECT_PROPOSAL(ACLMessage.REJECT_PROPOSAL, "REJECT-PROPOSAL"),

    /**
     * The sender requests the receiver to perform some action. Usually to
     * request the receiver to perform another communicative act.
     */
    REQUEST(ACLMessage.REQUEST, "REQUEST"),

    /**
     * The sender wants the receiver to perform some action when some given
     * proposition becomes true.
     */
    REQUEST_WHEN(ACLMessage.REQUEST_WHEN, "REQUEST-WHEN"),

    /**
     * The sender wants the receiver to perform some action as soon as some
     * proposition becomes true and thereafter each time the proposition becomes
     * true again.
     */
    REQUEST_WHENEVER(ACLMessage.REQUEST_WHENEVER, "REQUEST-WHENEVER"),

    /**
     * The act of requesting a persistent intention to notify the sender of the
     * value of a reference, and to notify again whenever the object identified
     * by the reference changes.
     */
    SUBSCRIBE(ACLMessage.SUBSCRIBE, "SUBSCRIBE");

    private final int code;
    private final String name;

    private Performative(int code, String name)
    {
        this.code = code;
        this.name = name;
    }

    /**
     * @return the {@link ACLMessage} code for this message type.
     */
    public int code() {
        return code;
    }

    /**
     * @return a convenient String representation of this performative
     */
    @Override
    public String toString() {
        return name;
    }
}
