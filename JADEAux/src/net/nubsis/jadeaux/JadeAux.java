package net.nubsis.jadeaux;

import jade.core.Profile;
import jade.core.ProfileImpl;
import jade.core.Runtime;
import jade.tools.rma.rma;
import jade.wrapper.AgentContainer;
import jade.wrapper.AgentController;
import jade.wrapper.ControllerException;
import jade.wrapper.StaleProxyException;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Collection;
import java.util.Collections;
import java.util.Map;
import java.util.TreeMap;

import net.nubsis.jadeaux.platform.PlatformListener;
import net.nubsis.jadeaux.platform.PlatformListenerAdapter;

/**
 * Very basic and ad-hoc helper class to "manually" load agents and containers
 * in a Jade environment.
 * 
 * @author Nubsis
 * 
 */
public class JadeAux {

	private static Runtime rt;

	private static ProfileImpl mainProfile = null;
	private static ProfileImpl containerProfile = null;

	private static Map<Integer, AgentContainer> containers;
	private static Map<String, AgentController> agents;

	private static Container main;

	public static void init() throws ControllerException {
		try {
			init(InetAddress.getLocalHost().getHostAddress(), 1200);
		} catch (UnknownHostException e) {
			throw new IllegalStateException("Local Host Address could not be resolved.", e);
		}
	}

	public static void init(String ip, Integer port) throws ControllerException {
		// Get a hold on JADE runtime
		rt = Runtime.instance();

		// Exit the JVM when there are no more containers around
		rt.setCloseVM(true);

		// Create default profile
		mainProfile = new ProfileImpl(true);
		mainProfile.setParameter(Profile.MAIN_HOST, ip);
		mainProfile.setParameter(Profile.MAIN_PORT, port.toString());
		mainProfile.setParameter(Profile.PLATFORM_ID, ip);

		// Create the main container
		main = new Container(rt.createMainContainer(mainProfile), 0);

		containerProfile = new ProfileImpl(false);
		containerProfile.setParameter(Profile.DETECT_MAIN, "*");
		containerProfile.setParameter(Profile.MAIN_HOST, ip);
		containerProfile.setParameter(Profile.MAIN_PORT, port.toString());
		containerProfile.setParameter(Profile.PLATFORM_ID, main.getPlatformName());

		// Why?
		main.addPlatformListener(new PlatformListenerAdapter());

		containers = new TreeMap<Integer, AgentContainer>();
		agents = new TreeMap<String, AgentController>();

		containers.put(0, main.getInternal());
	}

	public static void addPlatformListener(PlatformListener listener) throws ControllerException {
		main.addPlatformListener(listener);
	}

	public static void showGUI() throws StaleProxyException {
		// Create and start the rma
		Agent rma = main.createAgent(rma.class);
		rma.getInternal().start();
	}

	public static Container createContainer(ProfileImpl profile) {
		AgentContainer ac = rt.createAgentContainer(profile);
		Container c = new Container(ac, containers.size());
		containers.put(containers.size(), ac);
		return c;
	}

	public static Container createContainer(String name) {
		ProfileImpl profile = new ProfileImpl(containerProfile.getProperties());
		profile.setParameter(Profile.CONTAINER_NAME, name);
		return createContainer(profile);
	}

	public static Container createContainer() {
		return createContainer("Container-" + containers.size());
	}

	/**
	 * Creates and starts an agent.
	 * 
	 * @param agentClass
	 *            The class of the agent to be started
	 * @param name
	 *            The local name of the agent
	 * @param container
	 *            The container number in which to start the agent
	 * @param args
	 *            Arguments to initiate the agent with
	 * @throws StaleProxyException
	 */
	public static Agent createAgent(String name, Class<? extends jade.core.Agent> agentClass,
					Container c, Object... args) throws StaleProxyException {
		return createAgent(name, agentClass, c.getAgentContainer(), args);
	}

	public static Agent createAgent(String name, Class<? extends jade.core.Agent> agentClass,
					AgentContainer container, Object... args) throws StaleProxyException {
		AgentController ag = container.createNewAgent(name, agentClass.getCanonicalName(), args);
		//ag.start();
		Agent a = new Agent(ag);
		agents.put(name, ag);
		return a;
	}

	public static Agent createAgent(Class<? extends jade.core.Agent> agent,
					AgentContainer container, Object... args) throws StaleProxyException {
		String name = agent.getSimpleName();
		int i = 1;
		do {
			name = agent.getSimpleName() + "-" + i++;
		} while (agents.containsKey(name));
		System.out.println("Creating agent: " + name);

		return createAgent(name, agent, container, args);
	}

	public static Container getMainContainer() {
		return main;
	}

	public static void shutdown() throws StaleProxyException {
		if(main!=null)
		{
			main.createAgent(TerminatorAgent.class);
		}
	}

	public static Collection<AgentController> getAgents()
	{
		return Collections.unmodifiableCollection(agents.values());
	}

	public static Collection<AgentContainer> getContainers()
	{

		return Collections.unmodifiableCollection(containers.values());
	}
}
