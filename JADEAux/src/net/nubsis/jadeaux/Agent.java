package net.nubsis.jadeaux;

import jade.wrapper.AgentController;
import jade.wrapper.StaleProxyException;

public class Agent {

	private final AgentController internal;

	public Agent(AgentController ag) {
		internal = ag;
	}

	public AgentController getInternal() {
		return internal;
	}

	public void start() throws StaleProxyException
	{
		internal.start();
	}

}
