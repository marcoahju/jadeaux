package net.nubsis.jadeaux;

import jade.wrapper.ControllerException;
import jade.wrapper.StaleProxyException;
import net.nubsis.jadeaux.platform.PlatformListener;

public class JadeAuxApplication implements JadeAuxInterface {

    public JadeAuxApplication() throws ControllerException
    {
	this("192.168.1.64", 1200);
    }

    public JadeAuxApplication(String ip, int port) throws ControllerException
    {
	init(ip, port);
	setup();
    }

    // Final?	
    public void init() throws ControllerException
    {
	JadeAux.init(); 
    }

    public void showGUI() throws StaleProxyException
    {
	JadeAux.showGUI();
    }

    @Override
    public void init(String ip, int port) throws ControllerException
    {
	JadeAux.init(ip, port);
    }

    @Override
    public Agent createAgent(String agentName, Class<? extends jade.core.Agent> agentClass,
	    Container c, Object... args) throws StaleProxyException {
	return JadeAux.createAgent(agentName, agentClass, c, args);
    }

    @Override
    public Agent createAgent(Class<? extends jade.core.Agent> agentClass, Container c, Object... args) throws StaleProxyException {
	return JadeAux.createAgent(agentClass.getSimpleName(), agentClass, c, args);
    }

    @Override
    public Container createContainer(String name)
    {
	return JadeAux.createContainer(name);
    }

    @Override
    public Container createContainer()
    {
	return JadeAux.createContainer();
    }

    @Override
    public final void addPlatformListener(PlatformListener listener) throws ControllerException {
	JadeAux.getMainContainer().getInternal().addPlatformListener(listener);
    }

    @Override
    public void setup() throws StaleProxyException {

    }
}
