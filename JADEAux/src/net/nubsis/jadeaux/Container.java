package net.nubsis.jadeaux;

import jade.wrapper.AgentContainer;
import jade.wrapper.ControllerException;
import jade.wrapper.StaleProxyException;
import net.nubsis.jadeaux.platform.PlatformListener;

public class Container {

	private final AgentContainer internal;
	private final int index;
	
	Container(AgentContainer ac, int index)
	{
		this.internal = ac;
		this.index = index;
	}

	AgentContainer getAgentContainer() {
		return this.getInternal();
	}
	
	public Agent createAgent(String name, Class<? extends jade.core.Agent> agent,  Object... args) throws StaleProxyException
	{
		return JadeAux.createAgent(name, agent, this.internal, args);
	}
	
	public Agent createAgent(Class<? extends jade.core.Agent> agent, Object... args) throws StaleProxyException
	{
		return JadeAux.createAgent(agent, this.internal, args);
	}

	public String getPlatformName() {
		return getInternal().getPlatformName();
	}
	
	public String getContainerName() throws ControllerException
	{
		return getInternal().getContainerName();
	}

	public AgentContainer getInternal() {
		return internal;
	}

	public void addPlatformListener(PlatformListener listener) throws ControllerException {
		internal.addPlatformListener(listener);
	}

}
